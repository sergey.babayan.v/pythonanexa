import logging


logger = logging.getLogger(__name__)


class OrderingByViewMixin():
    allowed_sort_fields = []
    ordering_kwarg = "order"

    def _get_parametrs(self):
        return self.request.GET.getlist(self.ordering_kwarg)

    def get_ordering(self):
        ordering_list = self._get_parametrs()
        allowed_ordering_list = list(filter(lambda elem: elem.lstrip("-") in self.allowed_sort_fields, set(ordering_list)))
        logger.debug("Sorting queryset by %s" % str(allowed_ordering_list))
        return allowed_ordering_list