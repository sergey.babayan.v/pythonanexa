def get_or_none(model, **kwargs):
    queryset = model._default_manager.filter(**kwargs)
    if queryset.count() == 1:
        return queryset.first()
    else:
        return None

def slice_by_count(queryset, count):
    if queryset.count() > count:
        return queryset[:count]
    else:
        return queryset