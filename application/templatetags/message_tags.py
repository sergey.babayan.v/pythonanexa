from django import template


register = template.Library()


@register.filter
def filter_messge_tags(tags):
    return 'danger' if tags == 'error' else tags
