from django.contrib import admin
from django.urls import path, include

from .views import api as api_views
from .views import pages as pages_views


api_urlpatterns = [
    path("list/", api_views.ApplicationListAPIView.as_view(), name="create"),
    path("create/", api_views.ApplicationCreateAPIView.as_view(), name="create"),
    path("<int:id>/update/", api_views.ApplicationUpdateAPIView.as_view(), name="update"),

]

urlpatterns = [
    path("", pages_views.ApplicationListView.as_view(), name="list"),
    path("create/", pages_views.ApplicationCreateView.as_view(), name="create"),
    path("<int:id>/", pages_views.ApplicationDetailView.as_view(), name="detail"),
    path("<int:id>/edit/", pages_views.ApplicationEditView.as_view(), name="edit"),
    path("api/", include((api_urlpatterns, "application"), namespace="api"))
]
