import logging

from rest_framework import generics
from rest_framework.exceptions import ValidationError

from .. import models
from .. import serializers
from ..utils import get_or_none, slice_by_count


logger = logging.getLogger(__name__)


class ApplicationCreateAPIView(generics.CreateAPIView):
    queryset = models.Application.objects.all()
    serializer_class = serializers.ApplicationSerializer


class ApplicationUpdateAPIView(generics.UpdateAPIView):
    queryset = models.Application.objects.all()
    serializer_class = serializers.ApplicationSerializer
    lookup_field = "id"


class ApplicationListAPIView(generics.ListAPIView):
    serializer_class = serializers.ApplicationSerializer
    request_serializer_class = serializers.ApplicationListSerializer
    http_method_names = ["post"]

    def get_queryset(self):
        request_serializer = self.request_serializer_class(data=self.request.data)
        if request_serializer.is_valid():
            logger.info(f"Requested data {request_serializer.validated_data} is valid")
            queryset = slice_by_count(models.Application.objects.filter(id__gte=request_serializer.validated_data["id"]), request_serializer.validated_data["count"])
            return queryset
        else:
            logger.info(f"Requested data {self.request.data} is not-valid")
            raise ValidationError({"error":"Not provided needed data"})
        
    
    def post(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
