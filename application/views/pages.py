import logging

from django.shortcuts import render
from django.views.generic import ListView, CreateView, DetailView, UpdateView
from django.contrib import messages

from .. import models
from .. import forms as application_forms
from .. import mixins


logger = logging.getLogger(__name__)


class ApplicationDetailView(DetailView):
    template_name = "application/detail.html"
    model = models.Application
    context_object_name = "object"
    pk_url_kwarg = "id"
    form_class = application_forms.ApplicationModelForm
    form_context_name = "form"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        instace = context[self.context_object_name]
        form = self.form_class(instance=instace)
        context[self.form_context_name] = form
        return context


class ApplicationListView(mixins.OrderingByViewMixin, ListView):
    template_name = "application/list.html"
    context_object_name = "objects"
    paginate_by = 2
    model = models.Application
    ordering_kwarg = "order"
    page_kwarg = "page"
    allowed_sort_fields = ["created", "name", "till_date"]


class ApplicationCreateView(CreateView):
    template_name = "application/detail.html"
    model = models.Application
    context_object_name = "object"
    pk_url_kwarg = "id"
    form_class = application_forms.ApplicationModelForm
    form_context_name = "form"
    extra_context = {"create": True}

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.add_message(
        self.request, messages.SUCCESS, "Заявка добавлена")
        logger.info("Заявка добавлена")
        return result

    def form_invalid(self, form):
        result = super().form_valid(form)
        messages.add_message(
            self.request, messages.ERROR, "Ошибка при сохранении завки")
        logger.info("Ошибка при сохранении завки")
        return result


class ApplicationEditView(UpdateView):
    template_name = "application/detail.html"
    model = models.Application
    context_object_name = "object"
    pk_url_kwarg = "id"
    form_class = application_forms.ApplicationModelForm
    form_context_name = "form"
    extra_context = {"edit": True}

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.add_message(
        self.request, messages.SUCCESS, "Заявка сохранена")
        logger.info("Заявка сохранена")
        return result

    def form_invalid(self, form):
        result = super().form_valid(form)
        messages.add_message(
            self.request, messages.ERROR, "Ошибка при сохранении завки")
        logger.info("Ошибка при сохранении завки")
        return result
