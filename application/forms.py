from django import forms
from django.utils.safestring import mark_safe

from .models import Application


class SpanWidget(forms.Widget):

    def render(self, name, value, attrs=None, **kwargs):
        return mark_safe("<span>%s</span>" % (self.original_value))

    def value_from_datadict(self, data, files, name):
        if data:
            return None
        return self.original_value


class SpanField(forms.Field):

    def __init__(self, *args, **kwargs):
        kwargs['widget'] = kwargs.get('widget', SpanWidget)
        super().__init__(*args, **kwargs)


class Readonly(object):

    class ReadOnlyMeta:
        read_only = tuple()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        read_only = self.ReadOnlyMeta.read_only
        instance = kwargs.get("instance", None)
        if not read_only:
            return
        for name in read_only:
            field = self.fields[name]
            if not instance:
                del self.fields[name]
            field.widget = SpanWidget()
            label = self.Meta.labels.get(name, None)
            field.required = False
            field.value = None
            if label:
                field.label = label
            elif not isinstance(field, SpanField):
                continue
            field.widget.original_value = str(getattr(self.instance, name))


class ApplicationModelForm(Readonly, forms.ModelForm):
    created = forms.DateField(required=False)

    class Meta:
        model = Application
        fields = "__all__"
        labels = {"created": "Дата Создания"}
    
    class ReadOnlyMeta:
        read_only = ("created", )
