from django.db import models
from django.shortcuts import reverse

from . import validators


class Application(models.Model):
    STATUS_CHOICES = (
        ("open", "Open"),
        ("noffer", "Needs Offer"),
        ("offered", "Offered"),
        ("approved", "Approved"),
        ("inprogress", "In Progress"),
        ("ready", "Ready"),
        ("verified", "Verified"),
        ("closed", "Closed"),
    )

    created = models.DateTimeField(auto_now_add=True, blank=True)
    name = models.CharField(max_length=64, verbose_name="Наименование")
    image = models.ImageField(upload_to="images/applications/", verbose_name="Изображение", null=True, blank=True)
    text = models.TextField(verbose_name="Текст")
    till_date = models.DateField(verbose_name="Дата завершения", validators=[validators.validate_date_from_today])
    status = models.CharField(choices=STATUS_CHOICES, max_length=22, verbose_name="Статус")

    def __str__(self):
        return self.name
    
    def get_absolute_url(self):
        print(self)
        return reverse("application:detail", kwargs={"id": self.id})
    
    def get_edit_url(self):
        return reverse("application:edit", kwargs={"id": self.id})
    
    class Meta:
        ordering = ['id']