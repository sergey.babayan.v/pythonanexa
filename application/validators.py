from datetime import datetime
from html.parser import HTMLParser

from django.core.exceptions import ValidationError


def validate_date_from_today(value):
    if value < datetime.date(datetime.today()):
        raise ValidationError(
            "Значение %(value)s должно быть больше текущей даты",
            params={"value":value}
        )


def validate_positive_integer(value):
    if value < 0:
        raise ValidationError(
            "Значение %(value)s должно быть больше 0",
            params={"value":value}
        )