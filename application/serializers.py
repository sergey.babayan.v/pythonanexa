from rest_framework import serializers

from .models import Application
from .validators import validate_date_from_today, validate_positive_integer


class ApplicationSerializer(serializers.ModelSerializer):
    created = serializers.ReadOnlyField()
    till_date = serializers.DateField(validators=[validate_date_from_today])

    def create(self, validated_data):
        return Application.objects.create(**validated_data)

    def update(self, instace, validated_data):
        for field in validated_data:
            setattr(instace, field, validated_data[field])
            instace.save()
        return instace

    class Meta:
        model = Application
        fields = "__all__"


class ApplicationListSerializer(serializers.ModelSerializer):
    count = serializers.IntegerField(
        write_only=True,
        validators=[validate_positive_integer],
        required=True,
    )
    id = serializers.IntegerField(
        required=True
    )

    class Meta:
        model = Application
        fields = ("id", "count")
